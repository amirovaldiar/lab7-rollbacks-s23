import psycopg2


start_transaction = "START TRANSACTION"
end_transaction = "END TRANSACTION"
rollback_transaction = "ROLLBACK"
price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"


def get_connection():
    return psycopg2.connect(
        dbname="lab",
        user="postgres",
        password="password",
        host="localhost",
        port=5432
    )  # TODO add your values here

def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            # OPEN TRANSACTION
            cur.execute(start_transaction)

            # DECREASE BALANCE
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    # ROLLBACK IF ERROR
                    cur.execute(rollback_transaction)
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                # ROLLBACK IF ERROR
                cur.execute(rollback_transaction)
                raise Exception("Bad balance")

            # DECREASE STOCK
            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    # ROLLBACK IF ERROR
                    cur.execute(rollback_transaction)
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                # ROLLBACK IF ERROR
                cur.execute(rollback_transaction)
                raise Exception("Product is out of stock")
            # CLOSE TRANSACTION
            cur.execute(end_transaction)

